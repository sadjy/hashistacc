# Multi-AZ and Multi-Regions Hashicorp Stack (Consul/Nomad/Vault) on AWS

Inspired from [this project](https://github.com/scottwinkler/terraform-aws-nomad) with following features:
- Multi-AZ (cycling) and Single Region Nomad + Consul cluster (Consul Servers + Nomad Servers + Nomad Clients)
- Static numbers of servers. ASG groups for every "type" of server with min = max
- Automatic federation of Consul cluster via EC2 tags
- Load balancers for every "server type" + fabio


# Improvements I made
- Added Multi-regions capability with WAN automatic peering
- Added automatic scale-out of Nomad clients when resources (arbitrary) are full using a [custom cloudwatch metrics exporter](https://gitlab.com/sadjy/cloudwatch-nomad-summary-metrics) 
- Added Vault Servers to the stack + its load balancer (Vault is using Consul storage)
- Added auto-unseal of Vault cluster with AWS KMS
- Added automatic scale-down of Nomad clients after a period (arbitrary) of unallocated job
- Added Cloudfare DNS records generation for LB endpoints
- Added mTLS within the Consul cluster using Vault PKI capabilities
- Added mTLS within the Nomad cluster (servers + clients) using Vault PKI capabilities
- Every node now runs an Vault Agent (to communicating with the Vault CA) leveraging Auto-Auth
- Enabled Consul Connect for service-mesh
- Added automated Nomad ACL token generation and storage in Vault
- Added automated scale-in of Nomad clients when they stay idle (without running jobs) for a designated period

# How to use
First please refer to [this repository](https://gitlab.com/sadjy/packer-hashistacc-ami) in order to build the AMI required for this project.

First make sure you fill up the following environment variables to feed the AWS and Cloudflare providers:
AWS:
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
Cloudflare:
- `CLOUDFLARE_EMAIL`
- `CLOUDFLARE_API_KEY` / `CLOUDFLARE_API_TOKEN`

After cloning, feel free to change the AWS regions you wish to operate in `./main.tf` as well as the name conventions for the datacenters. Make sure to add your domain (Cloudflare zone) in `./variables.tf` as well as the name of your SSH keypair. 
Taking in account the current configuration, a 	`terraform init` > `terraform plan` > `terraform apply` will bootstrap the following infrastructure:
```
eu-west-3 - dc1:
- 3 Consul servers (running Consul only in server mode)
- 3 Nomad servers (running Consul in client mode and Nomad in server mode)
- 3 Nomad clients (running Consul in client mode and Nomad in client mode)
- 2 Vault servers (running Consul in client mode and Vault in server mode)
- 1 LB for the Consul UI reachable at consul.dc1.<domain.com>:8500
- 1 LB for the Nomad UI reachable at nomad.dc1.<domain.com>:4646
- 1 LB for the (eventuel) Fabio UI reachable at fabio.dc1.<domain.com>:9998 and (eventual) proxied services at fabio.dc1.<domain.com>:9999
us-east-2 - dc2```
- 3 Consul servers
- 3 Nomad servers
- 3 Nomad clients
- 2 Vault servers
- 1 LB for the Consul UI reachable at consul.dc2.<domain.com>:8500
- 1 LB for the Nomad UI reachable at nomad.dc2.<domain.com>:4646
- 1 LB for the (eventuel) Fabio UI reachable at fabio.dc2.<domain.com>:9998 and (eventual) proxied services at fabio.dc2.<domain.com>:9999
```
You're now ready to [launch some services](https://gitlab.com/sadjy/nomad-hashistacc-samples) on your newly born cluster.

# ToDO
- Leverage Terraform Cloud for workspaces instead of module of modules
- Expand to Multi-Cloud (Azure, GCP)
- Include SSH keypair creation in TF
- Adopt DRY approach for whole code (terragrunt)
- Clean up
