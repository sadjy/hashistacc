variable domain {
  default = "sadj.io"
}

variable "vault" {
  default = {
    version = "1.3.2",
    servers_count = 1,
    server_instance_type = "t2.micro"
  }
}

variable "ssh_keypair" {
  default = "TMP-proj"
  type    = string
}

